module ua.dp.krotov.converter {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;

    opens ua.dp.krotov.converter to javafx.fxml;
    exports ua.dp.krotov.converter;
    exports ua.dp.krotov.converter.controller;
    opens ua.dp.krotov.converter.controller to javafx.fxml;
}