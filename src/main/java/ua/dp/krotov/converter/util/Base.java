package ua.dp.krotov.converter.util;

public enum Base {
    Bin("Bin", 2),
    Oct("Oct", 8),
    Dec("Dec", 10),
    Hex("Hex", 16);

    private String name;
    private Integer value;

    Base(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
