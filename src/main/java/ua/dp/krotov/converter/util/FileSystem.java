package ua.dp.krotov.converter.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileSystem {

    private final static String CONVERTED = "converted";

    public static List<Integer> readFromFile(Path path, String delimiter) throws IOException {
        String content = Files.readString(path);
        return Arrays.stream(content.split(delimiter)).map(String::trim).map(Integer::parseInt).toList();
    }

    public static void saveToFile(List<String> values, Path path, String delimiter) throws IOException {
        Files.write(path,
                values.stream()
                        .map(String::valueOf)
                        .collect(Collectors.joining(delimiter))
                        .getBytes());
    }

    public static Path generateOutputPath(Path inputFilePath, String outputDir, Base base) {
        String convertedFilePath = outputDir + inputFilePath.getFileSystem().getSeparator() + CONVERTED + "_" + base.getName() + "_" + inputFilePath.getFileName().toString();
        return Path.of(convertedFilePath);
    }
}
