package ua.dp.krotov.converter.util;

public class Common {
    public static String removeLeadingZeroes(String input) {
        int index;
        for (index = 0; index < input.length() - 1; index++) {
            if (input.charAt(index) != '0') {
                break;
            }
        }
        return input.substring(index);
    }
}
