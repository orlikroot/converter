package ua.dp.krotov.converter.service;

import ua.dp.krotov.converter.util.Base;

import java.util.List;

public interface ConverterService {
    String convert(Integer value, Base base);
    List<String> convertList(List<Integer> values, Base base);
}
