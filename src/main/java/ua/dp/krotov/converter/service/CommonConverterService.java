package ua.dp.krotov.converter.service;

import ua.dp.krotov.converter.util.Base;

import java.util.List;
import java.util.stream.Collectors;

public class CommonConverterService implements ConverterService{
    @Override
    public String convert(Integer value, Base base) {

        String digits = "0123456789ABCDEF";
        if (value <= 0) return "0";
        String result = "";
        while (value > 0) {
            int digit = value % base.getValue();
            result = digits.charAt(digit) + result;
            value = value / base.getValue();
        }
        return result;
    }

    @Override
    public List<String> convertList(List<Integer> values, Base base) {
        return values.stream().map(value -> convert(value, base)).collect(Collectors.toList());
    }
}
