package ua.dp.krotov.converter.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ua.dp.krotov.converter.service.CommonConverterService;
import ua.dp.krotov.converter.service.ConverterService;
import ua.dp.krotov.converter.util.Base;
import ua.dp.krotov.converter.util.Common;
import ua.dp.krotov.converter.util.FileSystem;

import java.nio.file.Path;
import java.util.List;

public class ConverterController {

    private final static String DELIMITER = ",";
    @FXML
    protected Stage stage;
    private ConverterService converterService = new CommonConverterService();
    @FXML
    private TextField textField;
    @FXML
    private TextField inputDelimiter;
    @FXML
    private TextArea textArea;
    @FXML
    private Label inputFileLabel;
    @FXML
    private Label outputDirLabel;
    @FXML
    private CheckBox binCheckBox;
    @FXML
    private CheckBox octCheckBox;
    @FXML
    private CheckBox decCheckBox;
    @FXML
    private CheckBox hexCheckBox;

    //-------- MANUAL --------
    @FXML
    protected void onNumberButtonClick(ActionEvent event) {
        textField.setText(Common.removeLeadingZeroes(textField.getText() + ((Button) event.getSource()).getText()));
    }

    @FXML
    protected void onClearButtonClick(ActionEvent event) {
        textField.setText(textField.getPromptText());
        textArea.setText(textArea.getPromptText());
    }

    @FXML
    protected void onConvertButtonClick(ActionEvent event) {
        String input = textField.getText();

        String typeStr = ((Button) event.getSource()).getText();

        Base base = Base.Oct;
        {
            switch (typeStr) {
                case "Bin" -> base = Base.Bin;
                case "Oct" -> base = Base.Oct;
                case "Dec" -> base = Base.Dec;
                case "Hex" -> base = Base.Hex;
            }
        }

        textArea.setText(converterService.convert(Integer.parseInt(input), base));
    }

    //-------- AUTOMATION --------
    @FXML
    protected void onSelectInputFileButtonClick(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Resource File");
        inputFileLabel.setText(fileChooser.showOpenDialog(stage).getAbsolutePath());
    }

    @FXML
    protected void onSelectOutputDirButtonClick(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select Output Directory");
        outputDirLabel.setText(directoryChooser.showDialog(stage).getAbsolutePath());
    }

    @FXML
    protected void onConvertFileButtonClick(ActionEvent event) {

        if (inputFileLabel.getText().isEmpty()) {
            showError("No input file selected", "Please, select input file.");
        } else if (outputDirLabel.getText().isEmpty()) {
            showError("No output directory selected", "Please, select output directory.");
        } else if (!(binCheckBox.isSelected() || octCheckBox.isSelected() || decCheckBox.isSelected() || hexCheckBox.isSelected())) {
            showError("No conversion type selected", "Please, select at last one or more conversion types.");
        } else {
            try {
                Path filePath = Path.of(inputFileLabel.getText());
                String delimiter = getDelimiter();
                List<Integer> values = FileSystem.readFromFile(filePath, delimiter);

                if (binCheckBox.isSelected()) {
                    List<String> resultList = converterService.convertList(values, Base.Bin);
                    Path outputPath = FileSystem.generateOutputPath(filePath, outputDirLabel.getText(), Base.Bin);
                    FileSystem.saveToFile(resultList, outputPath, delimiter);
                }

                if (octCheckBox.isSelected()) {
                    List<String> resultList = converterService.convertList(values, Base.Oct);
                    Path outputPath = FileSystem.generateOutputPath(filePath, outputDirLabel.getText(), Base.Oct);
                    FileSystem.saveToFile(resultList, outputPath, delimiter);
                }

                if (decCheckBox.isSelected()) {
                    List<String> resultList = converterService.convertList(values, Base.Dec);
                    Path outputPath = FileSystem.generateOutputPath(filePath, outputDirLabel.getText(), Base.Dec);
                    FileSystem.saveToFile(resultList, outputPath, delimiter);
                }

                if (hexCheckBox.isSelected()) {
                    List<String> resultList = converterService.convertList(values, Base.Hex);
                    Path outputPath = FileSystem.generateOutputPath(filePath, outputDirLabel.getText(), Base.Hex);
                    FileSystem.saveToFile(resultList, outputPath, delimiter);
                }
                showSuccess("Success", "Convert done");
            } catch (Exception e) {
                showError("Convert error", e.getMessage());
            }
        }
    }

    private void showError(String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

    private void showSuccess(String header, String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

    private String getDelimiter() {
        if (inputDelimiter.getText().isEmpty()) {
            return DELIMITER;
        } else {
            return inputDelimiter.getText();
        }
    }
}